-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 06, 2019 at 09:20 PM
-- Server version: 5.7.26-0ubuntu0.18.04.1
-- PHP Version: 7.2.19-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `teguhpri_web`
--

-- --------------------------------------------------------

--
-- Table structure for table `my_category`
--

CREATE TABLE `my_category` (
  `id` int(11) NOT NULL,
  `category` varchar(30) NOT NULL,
  `category_slug` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `my_category`
--

INSERT INTO `my_category` (`id`, `category`, `category_slug`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'Teknologi', 'teknologi', '2019-07-05 08:55:27', '2019-07-05 07:24:03', 1, 1),
(2, 'Pendidikan', 'pendidikan', '2019-07-04 19:22:11', '2019-07-05 02:03:37', 1, 1),
(5, 'Pemrograman', 'pemrograman', '2019-07-04 17:01:33', '2019-07-05 01:56:22', 1, 1),
(6, 'Karakter', 'karakter', '2019-07-05 09:33:21', '2019-07-05 02:33:42', 1, 1),
(8, 'Alam', 'alam', '2019-07-05 10:28:26', '2019-07-05 03:28:26', 1, NULL),
(15, 'Agama', 'agama', '2019-07-05 23:05:55', '2019-07-05 16:05:55', 1, NULL),
(19, 'Program', 'program', '2019-07-05 23:21:24', '2019-07-05 16:21:24', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `my_log`
--

CREATE TABLE `my_log` (
  `user_id` int(11) NOT NULL,
  `activity` varchar(100) NOT NULL,
  `user_ip` varchar(16) NOT NULL,
  `user_agent` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `my_navigation`
--

CREATE TABLE `my_navigation` (
  `id` int(11) NOT NULL,
  `nav_name` varchar(100) NOT NULL,
  `nav_url` varchar(100) NOT NULL DEFAULT '#',
  `nav_status` enum('Y','N') NOT NULL DEFAULT 'Y',
  `nav_icon` varchar(20) DEFAULT NULL,
  `nav_slug` varchar(50) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `my_navigation`
--

INSERT INTO `my_navigation` (`id`, `nav_name`, `nav_url`, `nav_status`, `nav_icon`, `nav_slug`, `parent_id`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'Dashboard', '#', 'Y', 'fas fa-home', 'panel/dashboard', 0, '2019-06-30 02:35:00', NULL, 1, NULL),
(2, 'Lihat Website', '#', 'Y', 'fas fa-globe', '/', 0, '2019-06-01 09:21:36', NULL, 1, NULL),
(3, 'Blog', '#', 'Y', 'fas fa-paper-plane', 'panel/blog', 0, '2019-06-30 02:29:59', NULL, 1, NULL),
(4, 'Artikel', '#', 'Y', 'far fa-circle', 'panel/blog/artikel', 3, '2019-06-30 14:01:53', NULL, 1, NULL),
(5, 'Kategori Artikel', '#', 'Y', 'far fa-circle', 'panel/blog/kategori', 3, '2019-06-30 14:02:00', NULL, 1, NULL),
(6, 'Tag Artikel', '#', 'Y', 'far fa-circle', 'panel/blog/tag', 3, '2019-06-30 14:02:03', NULL, 1, NULL),
(7, 'Portfolio', '#', 'Y', 'fa fa-briefcase', 'panel/portfolio', 0, '2019-06-30 14:01:26', NULL, 1, NULL),
(8, 'Pengaturan', 'options', 'Y', 'fas fa-cog', 'panel/options', 0, '2019-06-30 02:30:10', NULL, 1, NULL),
(9, 'Pengaturan Umum', '#', 'Y', 'far fa-circle', 'panel/options/general', 8, '2019-06-30 14:02:05', NULL, 1, NULL),
(10, 'Manajemen Pengguna', '#', 'Y', 'far fa-circle', 'panel/options/users', 8, '2019-06-30 14:02:08', NULL, 1, NULL),
(11, 'Rekam Jejak', '#', 'Y', 'far fa-circle', 'panel/options/log', 8, '2019-06-30 14:02:10', NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `my_options`
--

CREATE TABLE `my_options` (
  `id` int(11) NOT NULL,
  `option_group` int(20) NOT NULL,
  `option_var` int(50) NOT NULL,
  `option_val` varchar(100) DEFAULT NULL,
  `option_def` varchar(100) NOT NULL,
  `option_desc` varchar(150) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `my_posts`
--

CREATE TABLE `my_posts` (
  `id` int(11) NOT NULL,
  `post_title` varchar(50) NOT NULL,
  `post_content` text NOT NULL,
  `post_type` enum('post','page','portfolio') NOT NULL,
  `post_status` enum('Y','N') NOT NULL,
  `post_slug` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `my_posts`
--

INSERT INTO `my_posts` (`id`, `post_title`, `post_content`, `post_type`, `post_status`, `post_slug`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(4, 'Lorem ipsum dolor sit amet, consectetur adipiscing', '<p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: \"Open Sans\", Arial, sans-serif; font-size: 14px;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras iaculis arcu non massa dignissim rhoncus. Sed hendrerit est nec dui bibendum mattis. Nullam porta mattis tempor. Quisque dignissim feugiat felis id convallis. Curabitur auctor eros purus, eu malesuada felis molestie at. Sed eu mauris ac tellus posuere bibendum. Etiam faucibus augue sem, eu sodales elit maximus vehicula. Integer convallis pretium venenatis. Suspendisse consectetur euismod leo. Mauris posuere aliquet dui non aliquam. Maecenas vulputate id lacus eu fermentum. Praesent accumsan interdum diam, in pharetra justo tristique at. Proin lacinia volutpat malesuada. Morbi tellus sapien, aliquet eu sollicitudin eget, maximus non sem.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: \"Open Sans\", Arial, sans-serif; font-size: 14px;\">In hac habitasse platea dictumst. Nulla eu purus nunc. Proin augue justo, dapibus eu risus eu, posuere pellentesque nisi. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec elementum imperdiet turpis id aliquet. Vestibulum ullamcorper quam a odio mattis tincidunt. Etiam vel enim ac nulla porta congue eget nec arcu. Pellentesque facilisis sed purus ac commodo. Aenean commodo ipsum quis est pellentesque porta.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: \"Open Sans\", Arial, sans-serif; font-size: 14px;\">Suspendisse vitae mattis lorem. Cras mollis tortor felis, et ultrices lorem mattis non. Vivamus magna massa, auctor eget dolor ac, accumsan sodales felis. Mauris tincidunt pellentesque leo ac vestibulum. Nam tellus purus, scelerisque eu eleifend sed, ultricies non massa. Pellentesque at sollicitudin ex. Nullam facilisis, enim a pulvinar scelerisque, mauris metus sagittis purus, eu vehicula ligula diam non mi. Fusce pharetra neque porttitor, feugiat odio vitae, dignissim est. Sed vehicula lacinia dictum. Donec vulputate ut lacus sit amet lobortis.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: \"Open Sans\", Arial, sans-serif; font-size: 14px;\">Nullam sagittis quam turpis, in fringilla ex ultricies et. Morbi mattis ex sit amet convallis ultrices. Donec molestie dapibus libero, id porttitor felis euismod et. Phasellus odio elit, lobortis scelerisque pretium a, volutpat sit amet lacus. Nulla condimentum rhoncus pretium. Vestibulum hendrerit mi ac blandit hendrerit. Nam luctus orci vel elit vestibulum, imperdiet ullamcorper velit sodales. Praesent commodo felis tortor, id bibendum odio faucibus ut. Nunc eu ante nisl.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: \"Open Sans\", Arial, sans-serif; font-size: 14px;\">Quisque id nisi vel justo efficitur tincidunt a nec urna. Integer euismod vel magna ac iaculis. Nullam blandit lectus eget velit imperdiet molestie. Maecenas ac augue suscipit, dapibus nulla quis, pellentesque dui. Praesent ante felis, varius vel porta vitae, pellentesque eget mi. Phasellus bibendum risus ut malesuada rutrum. Cras ac placerat neque. In hac habitasse platea dictumst. Suspendisse risus odio, lacinia sed commodo vestibulum, lacinia eu turpis. Mauris tincidunt molestie luctus. Nam eu nisi elit. Donec eget sapien sit amet leo mollis viverra.</p>', 'page', 'N', 'lorem-ipsum-dolor-sit-amet-consectetur-adipiscing', '2019-07-06 12:53:03', '2019-07-06 19:53:03', 1, 1),
(5, 'Lorem ipsum dolor sit amet, consectetur adipiscing', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse vitae nulla eget ante volutpat luctus nec sed lorem. Phasellus suscipit id erat ac vehicula. Mauris vel porttitor risus. Donec pretium lectus nec quam tristique, nec ultrices nibh maximus. Fusce fermentum rhoncus risus vitae euismod. Suspendisse ut urna vitae sapien tincidunt imperdiet. Quisque auctor finibus odio. Quisque faucibus nunc vitae tellus accumsan laoreet. In quis neque eros. Cras viverra mi tortor. Etiam aliquet imperdiet suscipit.\r\n\r\nSuspendisse eleifend ac dolor ac sagittis. Vestibulum suscipit euismod enim et tempor. Nam vehicula lectus hendrerit elit ultrices, sed blandit augue vulputate. Vivamus dapibus condimentum nibh eu tristique. Mauris pellentesque massa tortor, id varius eros aliquam a. Nulla quis erat sagittis turpis euismod sodales. Nam venenatis eros ac aliquam maximus. Nulla commodo lorem augue, id imperdiet tortor placerat vel. Sed eget eleifend diam, in pretium lectus.', 'page', 'N', 'lorem-ipsum-dolor-sit-amet-consectetur-adipiscing', '2019-07-06 12:46:15', '2019-07-06 19:46:15', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `my_relation_category`
--

CREATE TABLE `my_relation_category` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `my_relation_category`
--

INSERT INTO `my_relation_category` (`id`, `category_id`, `post_id`) VALUES
(62, 2, 5),
(63, 5, 5),
(66, 2, 4);

-- --------------------------------------------------------

--
-- Table structure for table `my_relation_tags`
--

CREATE TABLE `my_relation_tags` (
  `id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `my_relation_tags`
--

INSERT INTO `my_relation_tags` (`id`, `tag_id`, `post_id`) VALUES
(23, 1, 5),
(24, 2, 5),
(26, 6, 4);

-- --------------------------------------------------------

--
-- Table structure for table `my_tags`
--

CREATE TABLE `my_tags` (
  `id` int(11) NOT NULL,
  `tag` varchar(30) NOT NULL,
  `tag_slug` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `my_tags`
--

INSERT INTO `my_tags` (`id`, `tag`, `tag_slug`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'PHP', 'php', '2019-07-04 19:44:38', '2019-07-05 14:29:56', 1, 1),
(2, 'Ruby', 'ruby', '2019-07-04 19:38:10', '2019-07-05 09:39:53', 1, 1),
(4, 'Java', 'java', '2019-07-04 19:38:15', '2019-07-04 19:45:24', 1, 1),
(5, 'Javascript', 'javascript', '2019-07-05 09:40:07', NULL, 1, NULL),
(6, 'TypeScript', 'typescript', '2019-07-05 09:40:14', NULL, 1, NULL),
(8, 'CoffeeScript', 'coffeescript', '2019-07-05 10:28:12', '2019-07-05 10:37:56', 1, 1),
(11, 'NodeJs', 'nodejs', '2019-07-06 09:51:31', NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `my_users`
--

CREATE TABLE `my_users` (
  `id` varchar(50) NOT NULL,
  `user_fn` varchar(25) NOT NULL,
  `user_ln` varchar(25) NOT NULL,
  `user_profile` varchar(255) NOT NULL,
  `user_email` varchar(50) NOT NULL,
  `user_password` varchar(255) DEFAULT NULL,
  `user_level` enum('admin','writer') NOT NULL,
  `is_active` enum('Y','N') NOT NULL DEFAULT 'N',
  `oauth_provider` enum('google','facebook') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `my_users`
--

INSERT INTO `my_users` (`id`, `user_fn`, `user_ln`, `user_profile`, `user_email`, `user_password`, `user_level`, `is_active`, `oauth_provider`, `created_at`, `updated_at`) VALUES
('107667450472032885696', 'Budi', 'Prih Hastomo', 'https://lh6.googleusercontent.com/-VobUNf00Tvo/AAAAAAAAAAI/AAAAAAAAASA/-ghbZUFAVGo/s50/photo.jpg?sz=1000', 'budi.prihhastomo27@gmail.com', NULL, 'writer', 'N', 'google', '2019-06-04 16:21:11', NULL),
('114425695486699335168', 'Budi', 'Prih Hastomo', 'https://lh3.googleusercontent.com/-e30pOuN_K-M/AAAAAAAAAAI/AAAAAAAAAAA/ACHi3re-GipfaTh7rKWTfa3G4JIItt5-FQ/s50-mo/photo.jpg?sz=1000', 'budi.prihhastomo13@gmail.com', NULL, 'writer', 'N', 'google', '2019-06-22 03:45:50', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `my_category`
--
ALTER TABLE `my_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `my_navigation`
--
ALTER TABLE `my_navigation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `my_options`
--
ALTER TABLE `my_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `my_posts`
--
ALTER TABLE `my_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `my_relation_category`
--
ALTER TABLE `my_relation_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `post_id` (`post_id`);

--
-- Indexes for table `my_relation_tags`
--
ALTER TABLE `my_relation_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tag_id` (`tag_id`),
  ADD KEY `post_id` (`post_id`);

--
-- Indexes for table `my_tags`
--
ALTER TABLE `my_tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `my_users`
--
ALTER TABLE `my_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `my_category`
--
ALTER TABLE `my_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `my_navigation`
--
ALTER TABLE `my_navigation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `my_options`
--
ALTER TABLE `my_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `my_posts`
--
ALTER TABLE `my_posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `my_relation_category`
--
ALTER TABLE `my_relation_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `my_relation_tags`
--
ALTER TABLE `my_relation_tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `my_tags`
--
ALTER TABLE `my_tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
