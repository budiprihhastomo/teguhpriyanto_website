<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        // TODO Something in here :)
    }
    
    public function delete_data()
    {
        if($this->input->is_ajax_request())
        {

            $id = $this->input->post('id');
            $act = $this->input->post('action');
            $this->var = 
            [
                'action'    => $action = 3,
                'result'    => $result = ($this->Helper->DeleteData(get_name_table($act), ['id' => $id]) > 0 ? 'success' : 'error'),
                'msg'       => show_message(3, $result),
            ];
            log_print($action, get_name_table($act), $id);
            $this->output
            ->set_content_type('application/json','utf-8')
            ->set_output(json_encode($this->var, JSON_PRETTY_PRINT))
            ->_display();
            exit;
        }
    }
}