<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/* Route Untuk BackEnd */ 
$route['panel/dashboard'] = 'BackEnd/Panel';

/* Route Untuk API Datatables */
$route['api/datatables/(:any)'] = 'API/API_Datatables/$1';

/* API GLOBAL */
$route['api/([a-z]+)/delete'] = 'API/API_Datatables/delete_data';
$route['api/upload/upload_image_content'] = 'API/API_Upload/upload_image_content';
$route['api/upload/remove_image_content'] = 'API/API_Upload/remove_image_content';

/* Route Untuk BackEnd/Panel/Blog */
$route['panel/blog/artikel'] = 'BackEnd/Blog/Artikel';
$route['panel/blog/artikel/data'] = 'BackEnd/Blog/Artikel/data';
$route['panel/blog/artikel/data/(:num)'] = 'BackEnd/Blog/Artikel/data/$1';
$route['panel/blog/artikel/save'] = 'BackEnd/Blog/Artikel/save_data';
$route['panel/category/save'] = 'BackEnd/Blog/Kategori/save_category';
$route['panel/tag/save'] = 'BackEnd/Blog/Tag/save_tag';
$route['panel/blog/kategori'] = 'BackEnd/Blog/Kategori';
$route['panel/blog/kategori/save'] = 'BackEnd/Blog/Kategori/save_data';
$route['panel/blog/tag'] = 'BackEnd/Blog/Tag';
$route['panel/blog/tag/save'] = 'BackEnd/Blog/Tag/save_data';

/* Route Untuk BackEnd/Panel/Portfolio */
$route['panel/portfolio'] = 'BackEnd/Portfolio/Portfolio';
$route['panel/portfolio/data'] = 'BackEnd/Portfolio/Portfolio/data';
$route['panel/portfolio/data/(:num)'] = 'BackEnd/Portfolio/Portfolio/data/$1';
$route['panel/portfolio/save'] = 'BackEnd/Portfolio/Portfolio/save_data';

/* Route Untuk BackEnd/Panel/Pengaturan */
$route['panel/options/general'] = 'BackEnd/Pengaturan/General';
$route['panel/options/general/save'] = 'BackEnd/Pengaturan/General/save_data';
$route['panel/options/users'] = 'BackEnd/Pengaturan/Users';
$route['panel/options/users/data'] = 'BackEnd/Pengaturan/Users/data';
$route['panel/options/users/data/(:num)'] = 'BackEnd/Pengaturan/Users/data/$1';
$route['panel/options/log'] = 'BackEnd/Pengaturan/Log';