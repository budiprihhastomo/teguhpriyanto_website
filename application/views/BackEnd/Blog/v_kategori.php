      <div class="row">
        <div class="col-md-12">
          <div class="card card-outline card-info">
            <div class="card-header">
              <div class="row">
                <div class="col-sm-12 col-md-8">
                  <h3 class="card-title"><i class="fa fa-file mr-2"></i>Daftar Kategori</h3>
                  <!-- tools box -->
                </div>
                <div class="col-sm-12 col-md-4 text-right">
                  <div class="card-tools">
                    <button id="btn-tambah" class="btn btn-sm btn-primary"><i class="fas fa-plus mr-2"></i>Tambah</button>
                  </div>
                  <!-- /. tools -->
                </div>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body pad">
              <div class="mb-3">
              <div class="table-responsive">
              <table id="dt-show" class="table table-bordered table-striped" colspan="100%" width="100%">
                <thead>
                <tr>
                  <th>Kategori</th>
                  <th>Slug</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody></tbody>
              </table>
              </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.col-->
      </div>
      <!-- ./row -->

      <!-- Modal Action -->
      <div class="modal fade" id="modal-action">
        <div class="modal-dialog">
        <?=form_open('panel/blog/kategori/save','name="form-master"')?>
          <input type="hidden" name="id" value>
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Tambah Kategori</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="form-group">
                <label for="category">Kategori</label>
                <input type="text" class="form-control" name="category" id="category" placeholder="Masukan Kategori">
              </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button class="btn btn-default" data-dismiss="modal">Batal</button>
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
        <?=form_close()?>
      </div>
      <!-- /.modal -->