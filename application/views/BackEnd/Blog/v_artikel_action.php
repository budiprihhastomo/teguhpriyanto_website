      <?=form_open_multipart('panel/blog/artikel/save', 'name="form-master"')?>
      <?=(isset($data['content']) ? '<input type="hidden" name="id" value="'.$data['content']->id.'">' : '')?>
      <div class="row">
        <div class="col-md-8">
          <div class="card card-outline card-info">
            <div class="card-header">
              <h3 class="card-title"><i class="fa fa-file mr-2"></i>
                Konten Area
              </h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body pad">
              <div class="mb-3">
                <div class="form-group">
                  <input type="text" class="form-control" id="post_title" name="post_title" placeholder="Masukan Judul Artikel" <?=(isset($data['content']) ? 'value="'.$data['content']->post_title.'"' : '')?>>
                </div>
                <div class="form-group">
                  <textarea name="post_content" id="post_content" name="post_content" placeholder="Buat Konten Artikel Disini !"><?=(isset($data['content']) ? $data['content']->post_content : '')?></textarea>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.col-->
        <div class="col-md-4">
        <!-- Content Option -->
          <div class="card card-outline card-info">
            <div class="card-header">
              <h3 class="card-title"><i class="fa fa-cog mr-2"></i>
                Pengaturan Konten
              </h3>
              <!-- tools box -->
              <div class="card-tools">
                <button type="button" class="btn btn-tool btn-sm" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                  <i class="fas fa-minus"></i></button>
              </div>
              <!-- /. tools -->
            </div>
            <!-- /.card-header -->
            <div class="card-body pad">
              <div class="mb-3">
                <div class="form-group">
                  <label for="post_status">Status Artikel</label>
                  <select id="post_status" name="post_status" class="form-control select2">
                    <option></option>
                    <option value="Y" <?=(isset($data['content']) && $data['content']->post_status == 'Y' ? 'selected' : '')?>>Publikasi</option>
                    <option value="N" <?=(isset($data['content']) && $data['content']->post_status == 'N' ? 'selected' : '')?>>Arsipkan</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="post_type">Tipe Artikel</label>
                  <select id="post_type" name="post_type" class="form-control select2">
                    <option></option>
                    <option value="post" <?=(isset($data['content']) && $data['content']->post_type == 'post' ? 'selected' : '')?>>Posting</option>
                    <option value="page" <?=(isset($data['content']) && $data['content']->post_type == 'page' ? 'selected' : '')?>>Halaman</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="post_category">Kategori Artikel</label>
                  <select id="post_category" name="post_category[]" multiple="multiple" class="form-control">
                  <?php 
                  if(isset($data['option']['kategori']))
                  {
                    foreach($data['option']['kategori'] as $item)
                    {
                      echo '<option value="'.$item->category.'" selected>'.$item->category.'</option>';
                    }
                  }
                  ?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="post_tag">Tag Artikel</label>
                  <select id="post_tag" name="post_tag[]" multiple="multiple" class="form-control">
                  <?php 
                  if(isset($data['option']['tag']))
                  {
                    foreach($data['option']['tag'] as $item)
                    {
                      echo '<option value="'.$item->tag.'" selected>'.$item->tag.'</option>';
                    }
                  }
                  ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="card-footer">
              <div class="float-right">
                <a href="<?=base_url('panel/blog/artikel')?>" class="btn btn-sm btn-danger"><i class="fa fa-times mr-2"></i>Batal</a>
                <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-save mr-2"></i>Simpan</button>
              </div>
            </div>
          </div>
        </div>
        <!-- /.col-->
      </div>
      <!-- ./row -->
      <?=form_close()?>