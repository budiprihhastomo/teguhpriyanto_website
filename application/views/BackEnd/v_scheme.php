<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>AdminLTE 3 | Dashboard 2</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="<?=base_url('assets/plugins/fontawesome-free/css/all.min.css')?>">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?=base_url('assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')?>">
  <link rel="stylesheet" href="<?=base_url('assets/plugins/select2/css/select2.min.css')?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url('assets/css/adminlte.min.css')?>">
  <link rel="stylesheet" href="<?=base_url('assets/plugins/datatables/dataTables.bootstrap4.css')?>">
  <link rel="stylesheet" href="<?=base_url('assets/plugins/toastr/toastr.min.css')?>">
  <link rel="stylesheet" href="<?=base_url('assets/plugins/sweetalert2/sweetalert2.min.css')?>">
  <link rel="stylesheet" href="<?=base_url('assets/plugins/summernote/summernote-bs4.css')?>">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <style>
  .tagify--outside{
    border: 0;
    padding: 0;
    margin: 0;
  }
  .tagify__input--outside{
      display: block;
      max-width: 600px;
      border: 1px solid #DDD;
      margin-bottom: 1em;
  }
  </style>
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link"><i class="fas fa-globe mr-2"></i>teguhpriyanto.web.id</a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Messages Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-comments"></i>
          <span class="badge badge-danger navbar-badge">3</span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <a href="#" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <img src="<?=base_url('assets/img/user1-128x128.jpg')?>" alt="User Avatar" class="img-size-50 mr-3 img-circle">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  Brad Diesel
                  <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                </h3>
                <p class="text-sm">Call me whenever you can...</p>
                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
              </div>
            </div>
            <!-- Message End -->
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <img src="<?=base_url('assets/img/user8-128x128.jpg')?>" alt="User Avatar" class="img-size-50 img-circle mr-3">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  John Pierce
                  <span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>
                </h3>
                <p class="text-sm">I got your message bro</p>
                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
              </div>
            </div>
            <!-- Message End -->
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <img src="<?=base_url('assets/img/user3-128x128.jpg')?>" alt="User Avatar" class="img-size-50 img-circle mr-3">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  Nora Silvester
                  <span class="float-right text-sm text-warning"><i class="fas fa-star"></i></span>
                </h3>
                <p class="text-sm">The subject goes here</p>
                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
              </div>
            </div>
            <!-- Message End -->
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
        </div>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="<?=base_url('assets/img/AdminLTELogo.png')?>" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?=base_url('assets/img/user2-160x160.jpg')?>" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Teguh Priyanto</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <?php $i=0; foreach($nav_parent as $nav) { ?>
          <?php if($i == 2) { ?>
            <li class="nav-header">MANAJEMEN KONTEN</li>
          <?php } ?>
          <?php if($this->Helper->FetchRow('my_navigation', ['parent_id' => $nav->id])->num_rows() > 0) { ?>
          <li class="nav-item has-treeview <?=($this->uri->segment(2) == explode('/',$nav->nav_slug)[1] ? 'menu-open' : '')?>">
            <a href="#" class="nav-link">
              <i class="nav-icon fas <?=(isset($nav->nav_icon) ? $nav->nav_icon : '')?>"></i>
              <p>
                <?=$nav->nav_name?>
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
            <?php foreach($this->Helper->FetchRow('my_navigation', ['parent_id' => $nav->id])->result() as $nav_sub) { ?>
              <li class="nav-item">
                <a href="<?=base_url($nav_sub->nav_slug)?>" class="nav-link <?=($this->uri->segment(3) == explode('/',$nav_sub->nav_slug)[2] ? 'active' : '')?>">
                  <i class="far <?=(isset($nav_sub->nav_icon) ? $nav_sub->nav_icon : '')?> nav-icon"></i>
                  <p><?=$nav_sub->nav_name?></p>
                </a>
              </li>
          <?php } ?>
            </ul>
          </li>
          <?php } else { ?>
          <li class="nav-item">
            <a href="<?=base_url($nav->nav_slug)?>" class="nav-link <?=($this->uri->segment(2) == explode('/',$nav->nav_slug)[1] ? 'active' : '')?>">
              <i class="nav-icon fas <?=(isset($nav->nav_icon) ? $nav->nav_icon : '')?>"></i>
              <p><?=$nav->nav_name?></p>
            </a>
          </li>
          <?php } $i++; } ?>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"><?=judul_halaman()?></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
          <?=$breadcrumb?>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
      <?php (isset($content) ? $this->load->view($content) : '')?>
      <!-- /. content -->
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <strong>Copyright &copy; <?=date('Y')?> <a href="<?=base_url()?>">teguhpriyanto.web.id</a>.</strong>
    Created With <i class="fas fa-heart text-danger"></i> By Budi Prih Hastomo
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 1.0.0 (Alpha Version)
    </div>
  </footer>
</div>
</body>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="<?=base_url('assets/plugins/jquery/jquery.min.js')?>"></script>
<!-- Bootstrap -->
<script src="<?=base_url('assets/plugins/bootstrap/js/bootstrap.bundle.min.js')?>"></script>
<!-- overlayScrollbars -->
<script src="<?=base_url('assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')?>"></script>
<!-- AdminLTE App -->
<script src="<?=base_url('assets/js/adminlte.js')?>"></script>

<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="<?=base_url('assets/plugins/jquery-mousewheel/jquery.mousewheel.js')?>"></script>
<script src="<?=base_url('assets/plugins/raphael/raphael.min.js')?>"></script>
<script src="<?=base_url('assets/plugins/jquery-mapael/jquery.mapael.min.js')?>"></script>
<script src="<?=base_url('assets/plugins/datatables/jquery.dataTables.js')?>"></script>
<script src="<?=base_url('assets/plugins/datatables/dataTables.bootstrap4.js')?>"></script>
<!-- ChartJS -->
<!-- <script src="<?=base_url('assets/plugins/chart.js/Chart.min.js')?>"></script> -->
<script src="<?=base_url('assets/plugins/select2/js/select2.min.js')?>"></script>
<script src="<?=base_url('assets/plugins/ckeditor/ckeditor.js')?>"></script>
<script src="<?=base_url('assets/plugins/toastr/toastr.min.js')?>"></script>
<script src="<?=base_url('assets/plugins/sweetalert2/sweetalert2.min.js')?>"></script>
<script src="<?=base_url('assets/plugins/summernote/summernote-bs4.min.js')?>"></script>
<script>
var _BASE_URL = "<?=base_url()?>"
var _TABLE = "<?=($this->uri->segment(3) == NULL ? $this->uri->segment(2) : $this->uri->segment(3))?>"
</script>
<script src="<?=base_url('assets/js/backend/action.js')?>"></script> 
</html>
