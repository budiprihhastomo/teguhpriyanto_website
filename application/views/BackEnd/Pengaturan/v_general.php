        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Pengaturan Dasar</h3>
            </div>
        <!-- form start -->
                <?=form_open('panel/options/general/save','name="form-master"')?>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <div class="form-group">
                                <label for="website_name">Nama Website</label>
                                <input type="text" class="form-control" id="website_name" name="website_name" placeholder="Nama Website" value="<?=(is_null($data[0]->option_val) ? $data[0]->option_def : $data[0]->option_val)?>">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12">
                            <div class="form-group">
                                <label for="website_owner">Nama Pemilik Website</label>
                                <input type="text" class="form-control" id="website_owner" name="website_owner" placeholder="Nama Pemilik Website" value="<?=(is_null($data[1]->option_val) ? $data[1]->option_def : $data[1]->option_val)?>">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>

        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Pengaturan Lanjut</h3>
            </div>
        <!-- form start -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <div class="form-group">
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="is_under_maintenance" name="is_under_maintenance" value="<?=(is_null($data[2]->option_val) ? 'NULL' : 1)?>" <?=(is_null($data[2]->option_val) || $data[2]->option_val != "1"? '' : 'checked="true"')?>>
                                    <label class="custom-control-label" for="is_under_maintenance">Under Maintenance</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer float-right">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
              <?=form_close();?>
            </div>