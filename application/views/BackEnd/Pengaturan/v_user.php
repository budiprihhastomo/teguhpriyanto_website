    <div class="row">
        <div class="col-md-12">
          <div class="card card-outline card-info">
            <div class="card-header">
              <div class="row">
                <div class="col-sm-12 col-md-8">
                  <h3 class="card-title"><i class="fa fa-users mr-2"></i>Daftar Pengguna</h3>
                  <!-- tools box -->
                </div>
                <div class="col-sm-12 col-md-4 text-right">
                  <div class="card-tools">
                    <a href="<?=base_url('panel/options/users/data')?>" class="btn btn-sm btn-primary"><i class="fas fa-plus mr-2"></i>Tambah</a>
                  </div>
                  <!-- /. tools -->
                </div>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body pad">
              <div class="mb-3">
              <div class="table-responsive">
              <table id="dt-show" class="table table-bordered table-striped" colspan="100%" width="100%">
                <thead>
                <tr>
                  <th>Nama Pengguna</th>
                  <th>Profile</th>
                  <th>Email</th>
                  <th>Level</th>
                  <th>Status</th>
                  <th>Provider</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody></tbody>
              </table>
              </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.col-->
      </div>
      <!-- ./row -->