    <div class="row">
        <div class="col-12 col-md-8">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Data Pengguna</h3>
                </div>
            <!-- form start -->
                <form role="form">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label for="user_fn">Nama Depan</label>
                                    <input type="text" class="form-control" id="user_fn" name="user_fn" placeholder="Nama Depan" value="<?=(isset($data) ? $data->user_fn : '')?>">
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label for="user_ln">Nama Belakang</label>
                                    <input type="text" class="form-control" id="user_ln" name="user_ln" placeholder="Nama Belakang" value="<?=(isset($data) ? $data->user_ln : '')?>">
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label for="user_password">Password</label>
                                    <input type="password" class="form-control" id="user_password" name="user_password" placeholder="Password">
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label for="user_password">Konfirmasi Password</label>
                                    <input type="password" class="form-control" id="c_user_password" name="c_user_password" placeholder="Konfirmasi Password">
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label for="is_active">Status</label>
                                    <select id="is_active" name="is_active" class="form-control select2">
                                        <option></option>
                                        <option value="Y" <?=(isset($data) && $data->is_active == 'Y' ? 'selected' : '')?>>Aktif</option>
                                        <option value="N" <?=(isset($data) && $data->is_active == 'N' ? 'selected' : '')?>>Non-Aktif</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label for="user_level">Status</label>
                                    <select id="user_level" name="user_level" class="form-control select2">
                                        <option></option>
                                        <option value="admin" <?=(isset($data) && $data->user_level == 'admin' ? 'selected' : '')?>>Admin</option>
                                        <option value="writer" <?=(isset($data) && $data->user_level == 'writer' ? 'selected' : '')?>>Writer</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer float-right">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
                </div>
        </div>
    </div>