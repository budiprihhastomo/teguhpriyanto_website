<?php defined('BASEPATH') OR exit('No direct script access allowed');

if(!function_exists('judul_halaman'))
{
    function judul_halaman()
    {
        $CI =& get_instance();
        $segment = ($CI->uri->segment(3) == 'data' || $CI->uri->segment(3) == false ? $CI->uri->segment(2) : $CI->uri->segment(3));
        switch($segment)
        {
            case 'dashboard';
            $title = 'Dashboard';
            break;
            case 'artikel';
            $title = 'Artikel';
            break;
            case 'kategori';
            $title = 'Kategori';
            break;
            case 'tag';
            $title = 'Tag';
            break;
            case 'portfolio';
            $title = 'Portfolio';
            break;
            case 'general';
            $title = 'Pengaturan Umum';
            break;
            case 'users';
            $title = 'Manajemen Pengguna';
            break;
            case 'log';
            $title = 'Rekam Jejak';
            break;
        }
        return $title;
    }
}