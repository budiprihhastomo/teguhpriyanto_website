<?php defined('BASEPATH') OR exit('No direct script access allowed');
if(!function_exists('slugify'))
{
    function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
        // trim
        $text = trim($text, '-');
        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        // lowercase
        $text = strtolower($text);
        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);
        if (empty($text))
        {
            return 'n-a';
        }
        return $text;
    }
}

if(!function_exists('show_message'))
{
    function show_message($action, $result)
    {
        $act = '';
        switch($action)
        {
            case 1 :
                $act = 'menambah';
                break;
            case 2 :
                $act = 'memperbarui';
                break;
            case 3 :
                $act = 'menghapus';
                break;
        }

        switch($result)
        {
            case 'success' :
            $msg = 'Berhasil '.$act.' data dalam database.';
            break;
            case 'info' :
            $msg = 'Tidak ada kejadian'.$act.' data dalam database';
            break;
            case 'error' :
            $msg = 'Terjadi kesalahan ketika '.$act.' data dalam database.';
            break;
        }

        return $msg;
    }
}

if(!function_exists('get_name_table'))
{
    function get_name_table($url)
    {
        $table = '';
        switch($url)
        {
            case 'kategori':
                $table = 'my_category';
                break;
            case 'tag':
                $table = 'my_tags';
                break;
            case 'artikel':
                $table = 'my_posts';
                break;
            case 'portfolio':
                $table = 'my_posts';
                break;
            case 'users':
                $table = 'my_users';
                break;
        }
        return $table;
    }
}

if(!function_exists('log_print'))
{
    function log_print($action, $table, $id = false, $msg = false)
    {
        $CI =& get_instance();
        switch($action)
        {
            case 1:
            $do = 'INSERT';
            break;
            case 2:
            $do = 'UPDATE';
            break;
            case 3:
            $do = 'DELETE';
            break;
            case 4:
            $do = 'LOGIN';
            break;
            case 5:
            $do = 'LOGOUT';
            break;
        }

        $collect_data =
        [
            'user_id'       => 1, //REVIEW $CI->session->userdata('user_id'),
            'activity'      => $do.' => '.$table.' => '.(!$id ? $msg : $id),
            'user_ip'       => $CI->input->ip_address(),
            'user_agent'    => $CI->agent->platform().' | '.$CI->agent->browser().' | '.$CI->agent->version(),
            'date'          => date('Y/m/d H:i:s'),
        ];
        
        return $CI->Helper->InsertData('my_log',$collect_data);
    }
}