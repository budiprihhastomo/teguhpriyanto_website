<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_API_Datatables extends CI_Model
{
    private $PK = 'id';
    private $TCat = 'my_category';
    private $TTag = 'my_tags';
    private $TPos = 'my_posts';
    private $TUse = 'my_users';
    private $TPor = 'my_portfolio';

    // NOTE Model Datatables Untuk Artikel
    private function _query_artikel()
    {
        $c_filter = ['post_title','post_content',null,'id'];
        $c_search = ['post_title'];
        $order_default = [$this->PK => 'asc'];
        $this->db->where('post_type', 'post');
        $this->db->or_where('post_type', 'page');
        $this->_query_datatable($c_search);
    }

    // NOTE Model Datatables Untuk Tags
    private function _query_tag()
    {
        $c_filter = ['tag','tag_slug',null,'id'];
        $c_search = ['tag','tag_slug'];
        $order_default = [$this->PK => 'asc'];
        $this->_query_datatable($c_search);
    }

    // NOTE Model Datatables Untuk Kategori
    private function _query_kategori()
    {
        $c_filter = ['category','category_slug',null,'id'];
        $c_search = ['category','category_slug'];
        $order_default = [$this->PK => 'asc'];
        $this->_query_datatable($c_search);
    }

    // NOTE Model Datatables Untuk Portfolio
    private function _query_portfolio()
    {
        $c_filter = ['post_title','post_content',null,'id'];
        $c_search = ['post_title'];
        $order_default = [$this->PK => 'asc'];
        $this->db->where('post_type', 'portfolio');
        $this->_query_datatable($c_search);
    }

    // NOTE Model Datatables Untuk Pengaturan (Log_Activity)
    private function _query_users()
    {
        $c_filter = ['id','user_fn','user_ln','user_email','user_profile','user_level','is_active','oauth_provider','created_at'];
        $c_search = ['id','user_fn','user_ln','user_email','oauth_provider'];
        $order_default = ['created_at' => 'desc'];
        $this->_query_datatable($c_search);
    }

    // NOTE Model Datatables Untuk Pengaturan (Log_Activity)
    private function _query_log()
    {
        $c_filter = ['user_id','activity','user_ip','user_agent','date'];
        $c_search = ['activity','user_ip','user_agent'];
        $order_default = ['date' => 'desc'];
        $this->_query_datatable($c_search);
    }

    /* ===================================================================================== */
    // NOTE  (Required) Function Initialize DataTable
    public function _query_datatable($c_search)
    {
        // Initialize POST Method
        $search = $this->input->post('search[value]');
        $order = $this->input->post('order');
        $order_column = $this->input->post('order[0][column]'); 
        $order_column_dir = $this->input->post('order[0][dir]');

        // TODO Jika kolom pencarian tidak kosong -> Cari Sekarang !
        if(!is_null($search))
        {
            $i = 1;
            foreach($c_search as $item)
            {
                if($i === 1)
                {
                    $this->db->group_start();
                    $this->db->like($item, $search);
                }
                else $this->db->or_like($item, $search);

                if(count($c_search) == $i) $this->db->group_end();
                $i++;
            }
        }

        // TODO Jika Order Dipilih -> Urutkan Sekarang !
        if(isset($order))
        {
            $this->db->order_by($c_filter[$order_column], $order_column_dir);
        }
        else if(isset($order_default))
        {
            $ord = $order_default;
            $this->db->order_by(key($ord), $ord[key($ord)]);
        }
    }
    // NOTE (Required) Function untuk menjalankan query datatable
    public function get_datatables($q_action,$n_table)
    {
        $this->$q_action();
        $length = $this->input->post('length');
        $start = $this->input->post('start');
        if($length != 1) $this->db->limit($length, $start);
        return $this->db->get($n_table)->result();
    }
    
    // TODO Function untuk menghitung banyak row yang sudah difilter
    public function count_filtered($q_action,$n_table)
    {
        $this->$q_action();
        return $this->db->get($n_table)->num_rows();
    }

    // TODO Function untuk menghitung semua row yang ada pada table
    public function count_all($n_table, $filter = false)
    {
        if($filter) $this->db->where($filter);
        return $this->db->count_all_results($n_table);
    }
}