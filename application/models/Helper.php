<?php defined('BASEPATH') or exit('No direct script access allowed');

class Helper extends CI_Model
{
    public function FetchRow($table, $condition = false)
    {
        if($condition)
        {
            return $this->db->get_where($table, $condition);
        }
        else
        {
            return $this->db->get($table);
        }
    }

    public function FetchLikeRow($table, $term)
    {
        $this->db->like($term, 'both');
        return $this->db->get($table)->result();
    }

    public function InsertData($table, $data=[])
    {
        $this->db->insert($table,$data);
        return $this->db->affected_rows();
    }

    public function InsertDataBatch($table, $data=[])
    {
        $this->db->insert_batch($table,$data);
        return $this->db->affected_rows();
    }

    public function UpdateData($table, $data=[], $filter)
    {
        $this->db->update($table,$data,$filter);
        return $this->db->affected_rows();
    }

    public function DeleteData($table, $filter)
    {
        $this->db->delete($table, $filter);
        return $this->db->affected_rows();
    }

    // Buatan Function Specific Untuk Artikel
    public function GetOption($select,$tb,$tb_join,$filter_join,$filter_where)
    {
        $this->db->select($select);
        $this->db->join($tb_join, $filter_join, 'LEFT');
        return $this->db->get_where($tb,$filter_where)->result();
    }

    public function DeleteWhereNotIn($table, $field, $data, $condition)
    {
        $this->db->where_not_in($field, $data);
        $this->db->where($condition);
        return $this->db->delete($table);
    }
}
