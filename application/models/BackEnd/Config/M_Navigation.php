<?php defined('BASEPATH') or exit('No direct script access allowed');

class M_Navigation extends CI_Model
{
    public static $PK = 'id';
    public static $TB = 'my_navigation';
    public function get_parent_navigation()
    {
        return $this->db->get_where(self::$TB, ['parent_id', 0]);
    }
}
