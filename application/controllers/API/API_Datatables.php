<?php defined('BASEPATH') OR exit('No direct script access allowed');

class API_Datatables extends MY_Controller
{
    // Pembuatan Variable Untuk TableName Database
    private $PK = 'id';
    private $TCat = 'my_category';
    private $TTag = 'my_tags';
    private $TPos = 'my_posts';
    private $TUse = 'my_users';
    private $TPor = 'my_portfolio';

    public function __construct()
    {
        parent::__construct();
        // TODO Something in here :)
        $this->load->model('API/M_API_Datatables','DT');
    }

    public function ajx_data_artikel()
    {
        // TODO Untuk Posts :)
        $query = $this->DT->get_datatables('_query_artikel','my_posts');
        $data = [];
        foreach($query as $item)
        {
            $row = [
                $item->post_title,
                word_limiter(strip_tags($item->post_content), 20),
                '<a href="'.base_url('panel/blog/artikel/data/'.$item->id).'" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a>
                <button class="btn btn-sm btn-danger btn-hapus" data-id="'.$item->id.'"><i class="fa fa-trash"></i></button>'
            ];
            $data[] = $row;
        }

        $this->var = 
        [
            'draw' => $this->input->post('draw'),
            'recordsTotal' => $this->DT->count_all('my_posts'),
            'recordsFiltered' => $this->DT->count_filtered('_query_artikel','my_posts'),
            'data' => $data,
        ];
        $this->output
        ->set_content_type('application/json','utf-8')
        ->set_output(json_encode($this->var, JSON_PRETTY_PRINT))
        ->_display();
        exit;
    }
    
    public function ajx_data_tag()
    {
        // TODO Untuk Tags :)
        $query = $this->DT->get_datatables('_query_tag','my_tags');
        $data = [];
        foreach($query as $item)
        {
            $row = [
                $item->tag,
                $item->tag_slug,
                '<button class="btn btn-sm btn-info btn-edit" data-id="'.$item->id.'"><i class="fa fa-edit"></i></button>
                <button class="btn btn-sm btn-danger btn-hapus" data-id="'.$item->id.'"><i class="fa fa-trash"></i></button>'
            ];
            $data[] = $row;
        }

        $this->var = 
        [
            'draw' => $this->input->post('draw'),
            'recordsTotal' => $this->DT->count_all('my_tags'),
            'recordsFiltered' => $this->DT->count_filtered('_query_tag','my_tags'),
            'data' => $data,
        ];
        $this->output
        ->set_content_type('application/json','utf-8')
        ->set_output(json_encode($this->var, JSON_PRETTY_PRINT))
        ->_display();
        exit;
    }

    public function ajx_data_kategori()
    {
        // TODO Untuk Category :)
        $query = $this->DT->get_datatables('_query_kategori','my_category');
        $data = [];
        foreach($query as $item)
        {
            $row = 
            [
                $item->category,
                $item->category_slug,
                '<button class="btn btn-sm btn-info btn-edit" data-id="'.$item->id.'"><i class="fa fa-edit"></i></button>
                <button class="btn btn-sm btn-danger btn-hapus" data-id="'.$item->id.'"><i class="fa fa-trash"></i></button>'    
            ];
            $data[] = $row;
        }

        $this->var = 
        [
            'draw' => $this->input->post('draw'),
            'recordsTotal' => $this->DT->count_all('my_category'),
            'recordsFiltered' => $this->DT->count_filtered('_query_kategori','my_category'),
            'data' => $data,
        ];
        $this->output
        ->set_content_type('application/json','utf-8')
        ->set_output(json_encode($this->var, JSON_PRETTY_PRINT))
        ->_display();
        exit;
    }

    public function ajx_data_users()
    {
        // TODO Untuk Users :)
        $query = $this->DT->get_datatables('_query_users','my_users');
        $data = [];
        foreach($query as $item)
        {
            $row = 
            [
                $item->user_fn .' '. $item->user_ln,
                '<img src="'.$item->user_profile.'" class="img-fluid rounded-circle" width="80px"/>',
                $item->user_email,
                ucfirst($item->user_level),
                '<span class="badge badge-'.($item->is_active == 'Y' ? 'success' : 'danger').'">'.($item->is_active == 'Y' ? 'Aktif' : 'Non-Aktif').'</span>',
                '<span class="badge badge-success">'.ucfirst($item->oauth_provider).'</span>',
                '<a href="'.base_url('panel/options/users/data/'.$item->id).'" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a>
                <button class="btn btn-sm btn-danger btn-hapus" data-id="'.$item->id.'"><i class="fa fa-trash"></i></button>'
            ];
            $data[] = $row;
        }

        $this->var = 
        [
            'draw' => $this->input->post('draw'),
            'recordsTotal' => $this->DT->count_all('my_users'),
            'recordsFiltered' => $this->DT->count_filtered('_query_users','my_users'),
            'data' => $data,
        ];
        $this->output
        ->set_content_type('application/json','utf-8')
        ->set_output(json_encode($this->var, JSON_PRETTY_PRINT))
        ->_display();
        exit;
    }

    public function ajx_data_portfolio()
    {
        // TODO Untuk Porfolio :)
        $query = $this->DT->get_datatables('_query_portfolio','my_posts');
        $data = [];
        foreach($query as $item)
        {
            $row = 
            [
                $item->post_title,
                word_limiter($item->post_content,15),
                '<a href="'.base_url('panel/portfolio/data/'.$item->id).'" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a>
                <button class="btn btn-sm btn-danger btn-hapus" data-id="'.$item->id.'"><i class="fa fa-trash"></i></button>'    
            ];
            $data[] = $row;
        }

        $this->var = 
        [
            'draw' => $this->input->post('draw'),
            'recordsTotal' => $this->DT->count_all('my_posts',['post_type'=>'portfolio']),
            'recordsFiltered' => $this->DT->count_filtered('_query_portfolio','my_posts'),
            'data' => $data,
        ];
        $this->output
        ->set_content_type('application/json','utf-8')
        ->set_output(json_encode($this->var, JSON_PRETTY_PRINT))
        ->_display();
        exit;
    }

    public function ajx_data_log()
    {
        // TODO Untuk Porfolio :)
        $query = $this->DT->get_datatables('_query_log','my_log');
        $data = [];
        foreach($query as $item)
        {
            $row = 
            [
                $item->user_id,
                $item->activity,
                $item->user_ip,
                $item->user_agent,
                $item->date,
            ];
            $data[] = $row;
        }

        $this->var = 
        [
            'draw' => $this->input->post('draw'),
            'recordsTotal' => $this->DT->count_all('my_log'),
            'recordsFiltered' => $this->DT->count_filtered('_query_log','my_log'),
            'data' => $data,
        ];
        $this->output
        ->set_content_type('application/json','utf-8')
        ->set_output(json_encode($this->var, JSON_PRETTY_PRINT))
        ->_display();
        exit;
    }
}