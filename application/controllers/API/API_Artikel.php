<?php defined('BASEPATH') OR exit('No direct script access allowed');

class API_Artikel extends CI_Controller
{
    private $PK = 'id';
    private $TCat = 'my_category';
    private $TTag = 'my_tags';
    public function __construct()
    {
        parent::__construct();
        // TODO Something in here :)
    }

    public function ajx_data_category()
    {
        if($this->input->is_ajax_request())
        {
            $term = $this->input->get('query');
            $data = $this->Helper->FetchLikeRow($this->TCat, ['category' => $term]);
            $this->var = [];
            foreach($data as $row)
            {
                $this->var[] = [
                    'id' => $row->id,
                    'text' => $row->category,
                ];
            }
            $this->output
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($this->var, JSON_PRETTY_PRINT))
            ->_display();
            exit;
        }
    }

    public function ajx_data_tag()
    {
        if($this->input->is_ajax_request())
        {
            $term = $this->input->get('query');
            $data = $this->Helper->FetchLikeRow($this->TTag, ['tag' => $term]);
            $this->var = [];
            foreach($data as $row)
            {
                $this->var[] = [
                    'id' => $row->id,
                    'text' => $row->tag,
                ];
            }
            $this->output
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($this->var, JSON_PRETTY_PRINT))
            ->_display();
            exit;
        }
    }
}