<?php defined('BASEPATH') OR exit('No direct script access allowed');

class API_Upload extends MY_Controller
{
    // Pembuatan Variable Untuk TableName Database
    private $PK = 'id';
    private $TPos = 'my_posts';
    private $TUse = 'my_users';
    private $TPor = 'my_portfolio';

    public function __construct()
    {
        parent::__construct();
        // TODO Something in here :)
    }

    protected function conf_upload_image_content()
    {
        $config = 
        [
            'upload_path' => './assets/upload',
            'allowed_types' => 'jpg|jpeg|png|gif',
            'max_size' => 2048,
            'encrypt_name' => true,
        ];
        return $this->load->library('upload', $config);
    }

    protected function conf_resize_image_content($data)
    {
        $config = 
        [
            'image_library' => 'gd2',
            'source_image' => './assets/upload/'.$data,
            'file_permissions' => 0777,
            'quality' => '80%',
            'new_image' => './assets/upload/content/'.$data,
        ];
        $this->load->library('image_lib', $config);
        return $this->image_lib->resize();
    }

    public function upload_image_content()
    {
        if($this->input->is_ajax_request())
        {
            $this->conf_upload_image_content();
            if(!$this->upload->do_upload('image'))
            {
                $this->var =
                [
                    'status' => 'image_not_uploaded',
                    'msg' => $this->upload->display_errors()
                ];
            }
            else
            {
                $image_data = $this->upload->data();
                $this->conf_resize_image_content($image_data['file_name']);
                if(unlink($image_data['full_path']))
                echo base_url('assets/upload/content/'.$image_data['file_name']);
            }
        }
    }

    public function remove_image_content()
    {
        if($this->input->is_ajax_request())
        {
            $src = $this->input->post('src');
            if(unlink(str_replace(base_url(),'',$src)));
        }
    }
}