<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Portfolio extends CI_Controller
{
    private $PK = 'id';
    private $TNav = 'my_navigation';
    private $TPos = 'my_posts';
    private $TRCa = 'my_relation_category';
    private $TRTa = 'my_relation_tags';
    private $TCat = 'my_category';
    private $TTag = 'my_tags';
    public function __construct()
    {
        parent::__construct();
        // TODO Something in here :)
        $this->breadcrumbs->push('Panel', 'panel/dashboard#');
        $this->breadcrumbs->push('Portfolio', '#');
    }

    public function index()
    {
        $data =
        [
            'breadcrumb' => $this->breadcrumbs->show(),
            'nav_parent' => $this->Helper->FetchRow($this->TNav, ['parent_id' => 0])->result(),
            'content' => 'BackEnd/Portfolio/v_portfolio'
        ];
        $this->load->view('BackEnd/v_scheme', $data);
    }

    public function data()
    {
        $this->breadcrumbs->push((is_null($this->uri->segment(4)) ? 'Tambah' : 'Ubah').' Data','panel/blog/artikel/data');
        $id = $this->uri->segment(4);
        $data = 
        [
            'breadcrumb'    => $this->breadcrumbs->show(),
            'nav_parent'    => $this->Helper->FetchRow($this->TNav, ['parent_id' => 0])->result(),
            'data'          => 
            [
                'content'   => $this->Helper->FetchRow($this->TPos, ['id'=>$id,'post_type'=>'portfolio'])->row(),
                'option'    => 
                [
                    'kategori'  => $this->Helper->GetOption('T1.id,T2.category','my_relation_category T1','my_category T2','T1.category_id=T2.id',['post_id'=>$id]),
                    'tag'       => $this->Helper->GetOption('T1.id,T2.tag','my_relation_tags T1','my_tags T2','T1.tag_id=T2.id',['post_id'=>$id]),
                ] 
            ],
            'content'       => 'BackEnd/Portfolio/v_portfolio_action'
        ];
        $this->load->view('BackEnd/v_scheme', $data);
    }

    public function save_relation_category_and_tag($post_id)
    {
        $category = $this->input->post('post_category');
        $tag = $this->input->post('post_tag');

        $add_category = [];
        $add_tag = [];
        if(isset($category))
        {
            foreach($category as $item)
            {
                $id = $this->Helper->FetchRow($this->TCat, ['category'=>$item])->row()->id;
                if($this->Helper->FetchRow($this->TRCa, ['category_id'=>$id, 'post_id'=>$post_id])->num_rows() == 0) $add_category[] = ['category_id'=>$id,'post_id'=>$post_id];
            }
        }

        if(isset($tag))
        {
            foreach($tag as $item)
            {
                $id = $this->Helper->FetchRow($this->TTag, ['tag'=>$item])->row()->id;
                if($this->Helper->FetchRow($this->TRTa, ['tag_id'=>$id, 'post_id'=>$post_id])->num_rows() == 0) $add_tag[] = ['tag_id'=>$id,'post_id'=>$post_id];
            }
        }
        if(count($add_category) > 0) $this->Helper->InsertDataBatch($this->TRCa, $add_category);
        if(count($add_tag) > 0) $this->Helper->InsertDataBatch($this->TRTa, $add_tag);
        $this->remove_relation_category_and_tag($post_id);
    }

    public function remove_relation_category_and_tag($post_id)
    {
        $category = $this->input->post('post_category');
        $tag = $this->input->post('post_tag');

        if(isset($category))
        {
            $id = [];
            foreach($category as $item)
            {
                $id[] = $this->Helper->FetchRow($this->TCat, ['category'=>$item])->row()->id;
            }
            $this->Helper->DeleteWhereNotIn($this->TRCa, 'category_id', $id, ['post_id'=>$post_id]);
        }
        else $this->Helper->DeleteData($this->TRCa, ['post_id'=>$post_id]);

        if(isset($tag))
        {
            $id = [];
            foreach($tag as $item)
            {
                $id[] = $this->Helper->FetchRow($this->TTag, ['tag'=>$item])->row()->id;
            }
            $this->Helper->DeleteWhereNotIn($this->TRTa, 'tag_id', $id, ['post_id'=>$post_id]);
        }
        else $this->Helper->DeleteData($this->TRTa, ['post_id'=>$post_id]);
    }

    public function save_data()
    {
        if($this->input->is_ajax_request())
        {
            if($this->is_valid())
            {
                $id = $this->input->post('id');
                $data = $this->collect_data();
                if($id) 
                {
                    $data = array_merge($data, ['updated_at' => date('Y/m/d H:i:s'), 'updated_by' => 1]); //REVIEW Hanya Sementara (Perlu Diganti)
                    unset($data['created_at']);
                    unset($data['created_by']);
                }
                $this->var = 
                [
                    'action'    => $action = (!isset($id) ? 1 : 2),
                    'result'    => ($action == 1 ? $result = ($this->Helper->InsertData($this->TPos, $data) > 0 ? 'success' : 'error') : $result = ($this->Helper->UpdateData($this->TPos, $data, ['id'=>$id]) > 0 ? 'success' : 'error')),
                    'msg'       => show_message($action, $result),
                ];
                $post_id = (isset($id) ? $id : $this->db->insert_id());
                $this->save_relation_category_and_tag($post_id);
                log_print($action, $this->TPos, (isset($id) ? $id : $post_id));
            }
            else
            {
                $this->var =
                [
                    'result' => 'warning',
                    'msg' => validation_errors(),
                ];
            }
            $this->output
            ->set_content_type('application/json','utf-8')
            ->set_output(json_encode($this->var, JSON_PRETTY_PRINT))
            ->_display();
            exit;
        }
    }

    protected function collect_data()
    {
        return 
        [
            'post_title'    => $this->input->post('post_title'),
            'post_content'  => $this->input->post('post_content'),
            'post_status'   => $this->input->post('post_status'),
            'post_type'     => 'portfolio',
            'post_slug'     => slugify($this->input->post('post_title')),
            'created_at'    => date('Y/m/d H:i:s'),
            'created_by'    => 1 //REVIEW Perlu Diubah Karena Hanya Sementara
        ];
    }

    protected function is_valid()
    {
        $this->form_validation->set_rules('post_title', 'Judul Artikel', 'required|max_length[100]');
        $this->form_validation->set_rules('post_status', 'Status Artikel', 'trim|required');
        return $this->form_validation->run();
    }
}