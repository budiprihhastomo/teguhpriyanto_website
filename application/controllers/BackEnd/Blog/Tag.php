<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tag extends MY_Controller
{
    private $PK = 'id';
    private $TNav = 'my_navigation';
    private $TTag = 'my_tags';
    public function __construct()
    {
        parent::__construct();
        // TODO Something in here :)
        $this->breadcrumbs->push('Panel', 'panel/dashboard#');
        $this->breadcrumbs->push('Blog', 'panel/blog/tag#');
        $this->breadcrumbs->push('Tag', '#');
    }

    public function index()
    {
        $data = 
        [
            'breadcrumb' => $this->breadcrumbs->show(),
            'nav_parent' => $this->Helper->FetchRow($this->TNav, ['parent_id' => 0])->result(),
            'content' => 'BackEnd/Blog/v_tag.php',
        ];
        $this->load->view('BackEnd/v_scheme', $data);
    }

    public function save_data()
    {
        if($this->input->is_ajax_request())
        {
            if($this->is_valid())
            {
                $id = $this->input->post('id');
                $data = $this->collect_data();

                if(isset($id)) 
                {
                    $data = array_merge($data, ['updated_at' => date('Y/m/d H:i:s'), 'updated_by' => 1]); //REVIEW Hanya Sementara (Perlu Diganti)
                    unset($data['created_at']);
                    unset($data['created_by']);
                }

                $this->var = 
                [
                    'action'    => $action = (!isset($id) ? 1 : 2),
                    'result'    => ($action == 1 ? $result = ($this->Helper->InsertData($this->TTag, $data) > 0 ? 'success' : 'error') : $result = ($this->Helper->UpdateData($this->TTag, $data, ['id'=>$id]) > 0 ? 'success' : 'error')),
                    'msg'       => show_message($action, $result),
                ];         
                log_print($action, $this->TTag, (isset($id) ? $id : $this->db->insert_id()));
            }
            else
            {
                $this->var =
                [
                    'result' => 'warning',
                    'msg' => validation_errors(),
                ];
            }
            $this->output
            ->set_content_type('application/json','utf-8')
            ->set_output(json_encode($this->var, JSON_PRETTY_PRINT))
            ->_display();
            exit;
        }
    }

    public function save_tag()
    {
        if($this->input->is_ajax_request())
        {
            $collect_data = 
            [
                'id' => $id = $this->input->post('id'),
                'tag' => $tag = $this->input->post('text'),
                'tag_slug' => slugify($tag),
                'created_at' => date('Y/m/d H:i:s'),
                'created_by' => 1 //REVIEW Sementara
            ];
            if($this->Helper->FetchRow($this->TTag, ['id' => $id])->num_rows() == 0)
            {
                $this->Helper->InsertData($this->TTag, $collect_data);
                log_print(1, $this->TTag, (isset($id) ? $id : $this->db->insert_id()));
            }
        }
    }

    protected function collect_data()
    {
        return 
        [
            'tag' => $this->input->post('tag'),
            'tag_slug' => slugify($this->input->post('tag')),
            'created_at' => date('Y/m/d H:i:s'),
            'created_by' => 1,   //REVIEW Hanya Sementara (Perlu Diganti)
        ];
    }

    protected function is_valid()
    {
        $this->form_validation->set_rules('tag', 'Tag', 'required');
        return $this->form_validation->run();
    }
}