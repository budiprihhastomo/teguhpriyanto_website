<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends MY_Controller
{
    private $PK = 'id';
    private $TNav = 'my_navigation';
    private $TCat = 'my_category';
    public function __construct()
    {
        parent::__construct();
        // TODO Something in here :)
        $this->breadcrumbs->push('Panel', 'panel/dashboard#');
        $this->breadcrumbs->push('Blog', 'panel/blog/kategori#');
        $this->breadcrumbs->push('Kategori', '#');
    }

    public function index()
    {
        $data = 
        [
            'breadcrumb' => $this->breadcrumbs->show(),
            'nav_parent' => $this->Helper->FetchRow($this->TNav, ['parent_id' => 0])->result(),
            'content' => 'BackEnd/Blog/v_kategori.php'
        ];
        $this->load->view('BackEnd/v_scheme', $data);
    }

    public function save_data()
    {
        if($this->input->is_ajax_request())
        {
            if($this->is_valid())
            {
                $id = $this->input->post('id');
                $data = $this->collect_data();

                if($id) 
                {
                    $data = array_merge($data, ['updated_at' => date('Y/m/d H:i:s'), 'updated_by' => 1]); //REVIEW Hanya Sementara (Perlu Diganti)
                    unset($data['created_at']);
                    unset($data['created_by']);
                }

                $this->var = 
                [
                    'action'    => $action = (!isset($id) ? 1 : 2),
                    'result'    => ($action == 1 ? $result = ($this->Helper->InsertData($this->TCat, $data) > 0 ? 'success' : 'error') : $result = ($this->Helper->UpdateData($this->TCat, $data, ['id'=>$id]) > 0 ? 'success' : 'error')),
                    'msg'       => show_message($action, $result),
                ];
                log_print($action, $this->TCat, (isset($id) ? $id : $this->db->insert_id()));
            }
            else
            {
                $this->var =
                [
                    'result' => 'warning',
                    'msg' => validation_errors(),
                ];
            }
            $this->output
            ->set_content_type('application/json','utf-8')
            ->set_output(json_encode($this->var, JSON_PRETTY_PRINT))
            ->_display();
            exit;
        }
    }

    public function save_category()
    {
        if($this->input->is_ajax_request())
        {
            $collect_data = 
            [
                'id' => $id = $this->input->post('id'),
                'category' => $category = $this->input->post('text'),
                'category_slug' => slugify($category),
                'created_at' => date('Y/m/d H:i:s'),
                'created_by' => 1 //REVIEW Sementara
            ];
            if($this->Helper->FetchRow($this->TCat, ['id' => $id])->num_rows() == 0)
            {
                $this->Helper->InsertData($this->TCat, $collect_data);
                log_print(1, $this->TCat, (isset($id) ? $id : $this->db->insert_id()));
            }
        }
    }

    protected function collect_data()
    {
        return 
        [
            'category' => $this->input->post('category'),
            'category_slug' => slugify($this->input->post('category')),
            'created_at' => date('Y/m/d H:i:s'),
            'created_by' => 1,   //REVIEW Hanya Sementara (Perlu Diganti)
        ];
    }

    protected function is_valid()
    {
        $this->form_validation->set_rules('category', 'Kategori', 'required');
        return $this->form_validation->run();
    }
}