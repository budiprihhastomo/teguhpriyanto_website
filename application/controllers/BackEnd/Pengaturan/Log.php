<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Log extends CI_Controller
{
    private $PK = 'id';
    private $TNav = 'my_navigation';
    public function __construct()
    {
        parent::__construct();
        // TODO Something in here :)
        $this->breadcrumbs->push('Panel', 'panel/dashboard#');
        $this->breadcrumbs->push('Pengaturan', 'panel/options/log#');
        $this->breadcrumbs->push('Rekam Jejak', '#');
    }

    public function index()
    {
        $data = 
        [
            'breadcrumb' => $this->breadcrumbs->show(),
            'nav_parent' => $this->Helper->FetchRow($this->TNav, ['parent_id' => 0])->result(),
            'content'    => 'BackEnd/Pengaturan/v_log',
        ];
        $this->load->view('BackEnd/v_scheme', $data);
    }
}