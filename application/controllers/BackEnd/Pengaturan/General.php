<?php defined('BASEPATH') OR exit('No direct script access allowed');

class General extends CI_Controller
{
    private $PK = 'id';
    private $TNav = 'my_navigation';
    private $TOpt = 'my_options';    
    public function __construct()
    {
        parent::__construct();
        // TODO Something in here :)
        $this->breadcrumbs->push('Panel', 'panel/dashboard#');
        $this->breadcrumbs->push('Pengaturan', 'panel/options/general#');
        $this->breadcrumbs->push('Pengaturan Umum', '#');
    }

    public function index()
    {
        $data = 
        [
            'breadcrumb' => $this->breadcrumbs->show(),
            'nav_parent' => $this->Helper->FetchRow($this->TNav, ['parent_id' => 0])->result(),
            'data'  => $this->Helper->FetchRow($this->TOpt)->result(),
            'content'   => 'BackEnd/Pengaturan/v_general',
        ];
        $this->load->view('BackEnd/v_scheme', $data);
    }

    public function save_data()
    {
        if($this->input->is_ajax_request())
        {
            $id = $this->input->post('id');
            $data = $this->collect_data();
            $proccess_update = [
                $this->Helper->UpdateData($this->TOpt, ['option_val' => $data['option_var']['website_name']], ['option_var'=>'website_name']),
                $this->Helper->UpdateData($this->TOpt, ['option_val' => $data['option_var']['owner_name']], ['option_var'=>'owner_name']),
                $this->Helper->UpdateData($this->TOpt, ['option_val' => $data['option_var']['is_under_maintenance']], ['option_var'=>'is_under_maintenance']),
            ];

            $this->var = 
            [
                'action'    => $action = 2,
                'result'    => $result = (in_array(1, $proccess_update) ? 'success' : 'info'),
                'msg'       => show_message($action, $result),
            ];
            log_print($action, $this->TOpt, false, 'Peraturan Website Diubah');
            $this->output
            ->set_content_type('application/json','utf-8')
            ->set_output(json_encode($this->var, JSON_PRETTY_PRINT))
            ->_display();
            exit;
        }
    }

    protected function collect_data()
    {
        return 
        [
            'option_var' => 
            [
                'website_name' => $this->input->post('website_name'),
                'owner_name' => $this->input->post('website_owner'),
                'is_under_maintenance' => $this->input->post('is_under_maintenance'),
            ],
            'updated_at' => date('Y/m/d H:i:s'),
            'updated_by' => 1,   //REVIEW Hanya Sementara (Perlu Diganti)
        ];
    }
}
