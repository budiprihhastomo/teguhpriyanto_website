<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller
{
    private $PK = 'id';
    private $TNav = 'my_navigation';
    private $TUse = 'my_users';
    public function __construct()
    {
        parent::__construct();
        // TODO Something in here :)
        $this->breadcrumbs->push('Panel', 'panel/dashboard#');
        $this->breadcrumbs->push('Pengaturan', 'panel/options/users#');
        $this->breadcrumbs->push('Manajemen Pengguna', '#');
    }

    public function index()
    {
        $data = 
        [
            'breadcrumb' => $this->breadcrumbs->show(),
            'nav_parent' => $this->Helper->FetchRow($this->TNav, ['parent_id' => 0])->result(),
            'content'   => 'BackEnd/Pengaturan/v_user',
        ];
        $this->load->view('BackEnd/v_scheme', $data);
    }

    public function data()
    {
        $this->breadcrumbs->push((is_null($this->uri->segment(5)) ? 'Tambah' : 'Ubah').' Data','panel/blog/artikel/data');
        $id = $this->uri->segment(5);
        $data = 
        [
            'breadcrumb'    => $this->breadcrumbs->show(),
            'nav_parent'    => $this->Helper->FetchRow($this->TNav, ['parent_id' => 0])->result(),
            'data'          => $this->Helper->FetchRow($this->TUse, ['id'=>$id])->row(),
            'content'       => 'BackEnd/Pengaturan/v_user_action'
        ];
        $this->load->view('BackEnd/v_scheme', $data);
    }
}