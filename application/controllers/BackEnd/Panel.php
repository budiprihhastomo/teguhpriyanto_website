<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Panel extends CI_Controller
{
    private $PK = 'id';
    private $TNav = 'my_navigation';
    public function __construct()
    {
        parent::__construct();
        // TODO Something in here :)
        $this->breadcrumbs->push('Panel', 'panel/dashboard#');
        $this->breadcrumbs->push('Dashboard', '#');
    }

    public function index()
    {
        $data = 
        [
            'breadcrumb' => $this->breadcrumbs->show(),
            'nav_parent' => $this->Helper->FetchRow($this->TNav, ['parent_id' => 0])->result(),
            'content' => 'BackEnd/v_dashboard'
        ];
        $this->load->view('BackEnd/v_scheme', $data);
    }
}