$(document).ready(function(){
    dropdownSelect2('#post_status', 'Pilih Status')
    dropdownSelect2('#post_type', 'Pilih Tipe')
    tagsSelect2('#post_category', 'Pilih Kategori', 'category')
    tagsSelect2('#post_tag', 'Pilih Tag', 'tag')
  })

  // Function Upload Dan Remove Image Summernote
  function uploadImage(file){
    var data = new FormData()
    data.append('image', file)
    $.ajax({
      data: data,
      type: 'POST',
      contentType: false,
      processData: false,
      cache: false,
      url: _BASE_URL + 'api/upload/upload_image_content',
      success: function(url){
        $('#post_content').summernote("insertImage", url)
      }
    })
  }
  function deleteImage(src){
    $.ajax({
      data: {src:src},
      type: 'POST',
      url: _BASE_URL + 'api/upload/remove_image_content',
      cache: false,
    })
  }

  // Configurasi Dasar Untuk Toastr
  toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": true,
    "progressBar": true,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  }

  //  Function untuk mendapatkan data (Datatables)
  var get_datatable = function(el,table){
    return $(el).DataTable({
      responsive: true,
      processing: true,
      serverSide: true,
      order: [],
      ajax: {
        url: _BASE_URL + 'api/datatables/ajx_data_' + table,
        type: 'post',
        dataType: 'json',
        complete: function()
        {
          $('button.btn-edit').click(function(){
            var id = $(this).data('id')
            var closestRow = $(this).closest('tr')
            var data = dt.row(closestRow).data()
            $('input[name="id"]').prop('disabled', false)
            $('input[name="id"]').val(id)
            $('#category').val(data[0])
            $('#tag').val(data[0])
            $('#modal-action').modal('toggle')
          })

          $('button.btn-hapus').on('click', function(){
            Swal.fire({
              title: 'Apakah anda yakin?',
              text: "Data yang dihapus tidak dapat dikembalikan!",
              type: 'warning',
              showCancelButton: true,
              confirmButtonText: 'Hapus',
              cancelButtonColor: '#d33',
              confirmButtonColor: '#3085d6',
            }).then((result) => {
              if (result.value) {
                var id = $(this).data('id')
                $.post(_BASE_URL + 'api/'+ table +'/delete', {id:id,action:_TABLE})
                .done(function(obj){
                  Swal.fire('Hapus Data!',obj.msg,'success')
                  dt.ajax.reload()
                })
              }
            })
          })
        }
      }
    })
  }

  var post_content = $('#post_content')
  post_content.summernote({
    height: 391,
    callbacks: {
      onImageUpload: function(files){
        uploadImage(files[0])
      },
      onMediaDelete: function(target){
        deleteImage(target[0].src)
      }
    }
  })

  // Form Action
  $('form[name="form-master"]').submit(function(e){
    e.preventDefault()
    var formData = new FormData($(this)[0]);
    $.ajax({
      url: $(this).attr('action'),
      type: 'POST',
      dataType: 'json',
      processData: false,
      contentType: false,
      data: formData,
    })
    .done(function(obj){
      $('#modal-action').modal('toggle')
      if(obj.result == 'success')
      {
        if(obj.action == 1) 
        {
          $('input').val('')
          $('select').val('').trigger('change')
          post_content.summernote('reset')
        }
        toastr['success'](obj.msg, 'Berhasil !')
      }
      else if(obj.result == 'info')
      {
        toastr['info'](obj.msg, 'Tidak Ada Aktivitas !')
      } 
      else if(obj.result == 'warning')
      {
        toastr['warning'](obj.msg, 'Ada Sesuatu !')
      }
      else if(obj.result == 'error')
      {
        toastr['error'](obj.msg, 'Terjadi Kesalahan !')
      }
      dt.ajax.reload(null,false);
    })
  })

  $('#btn-tambah').on('click', function(){
    $('form[name="form-master"]')[0].reset()
    $('input[name="id"]').prop('disabled', true)
    $('#modal-action').modal('toggle')
  })

  var dt = get_datatable('#dt-show', _TABLE)

  // Function untuk initialize Dropdown Select2
  function dropdownSelect2(el,ph = false)
  {
    $(el).select2({
      placeholder: ph
    })
  }
  
  // Function untuk initialize Tag Select2 
  function tagsSelect2(el,placeholder = false,action)
  {
    $(el).select2({
      tags: true,
      placeholder: placeholder,
      tokenSeparatos: [',',' '],
      createSearchChoice: function(term, data) {
          if($(data).filter(function(){
              return this.text.localCompare(term) === 0
          }).length === 0) {
              return {
                  id: term,
                  text: term, 
              }
          }
      },
      multiple: true,
      ajax: {
          url: _BASE_URL + 'API/API_Artikel/ajx_data_'+ action,
          dataType: 'json',
          delay: 800,
          data: function(filter){
              return {
                query: filter.term
              }
          },
          processResults: function (data) {
            return {
                results: $.map(data, function(obj) {
                    return { id: obj.text, text: obj.text, idObj:obj.id };
                }),
            }
        }
      }
    }).on('select2:select', function(e){
      var data = e.params.data
      $.post(_BASE_URL + 'panel/' + action + '/save', {id:data.idObj,text:data.text})
    })
  }